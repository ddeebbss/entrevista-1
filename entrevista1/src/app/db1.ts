import { Products } from './dbproducts';


export const PRODUCTS: Products[] = [

  {
    id: 'GR1',
    name: 'Green Tea',
    price: 3.11,
    image: ['../assets/images/green-tea.jpg'],
    counterCart:0
  },
  {
    id: 'SR1',
    name: 'Strawberries',
    price: 5.00,
    image: ['../assets/images/strawberry.jpg'],
    counterCart:0
  },
  {
    id: 'CF1',
    name: 'Coffe',
    price: 11.23,
    image: ['../assets/images/coffee.jpg'],
    counterCart:0
  }
];
