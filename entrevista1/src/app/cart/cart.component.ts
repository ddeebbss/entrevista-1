import { Component, OnInit } from '@angular/core';
import { PRODUCTS } from '../db1';
import { ProductsService } from '../products.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  products = PRODUCTS;
  cartsProducts = []

  constructor(
    public productsService : ProductsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cartsProducts =this.productsService.getCartsProducts();
  }

  toggleCartPlus(product) {
    this.productsService.addProductCart(product);
  }

  toggleCartLess(product) {
    this.productsService.removeProductCart(product);
  }
}
