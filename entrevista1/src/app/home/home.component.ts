import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PRODUCTS } from '../db1';
import { ProductsService } from '../products.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  products = PRODUCTS;

  constructor(
    public productsService : ProductsService,
    private router: Router
  ) { }
  
  
  ngOnInit() {
  }

  toggleCartPlus(product) {
      this.productsService.addProductCart(product);
  }

  goToCart(){
    this.router.navigate(["/cart"]);
    console.log("Me voy a cart");
  }
}
