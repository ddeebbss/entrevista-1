import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  cartProduct=[];

  constructor() { }

  addProductCart(product) {
    if (product.counterCart == 0){
      this.cartProduct.push(product);
      product.counterCart++;
      console.log("El producto no estaba en carrito,lo añadimos")
    }else{
      product.counterCart++;
      console.log("sumamos uno al contador de carrito de este producto");
    }
  }

  removeProductCart(product){
    if (product.counterCart == 0){
      console.log("No se puede eliminar un producto que no esta en el carro ")
    }else{
      product.counterCart--;
    }
  }

  getCartsProducts() {
    return this.cartProduct;
  }
}
