
export class Products {
    id: string;
    name: string;
    price: number;
    image: any;
    counterCart: number;
}
